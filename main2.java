package first;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class main2 {
    public static void main(String[] args) throws IOException {
        DecimalFormat num = new DecimalFormat("##0.00");//创建格式化3位整数2位小数
        FileInputStream et = new FileInputStream("src/Gone_with_the_wind.txt");//读取文件 return值为int
        InputStreamReader reader = new InputStreamReader(et, "gbk");//读取字节 编码转换为gpk
        StringBuffer word = new StringBuffer();
        while (reader.ready()) {
            word.append((char) reader.read());
        }
        reader.close();
        et.close();


        int i;
        String A =word.toString();
        String letter = "abcdefghijklmnopqrstuvwxyz";
        String temp = "";
        char NUM[] = new char[A.length()];
        char Z[] = new char[26];
        int X[] = new int[26];
        int MAX = 0;
        Z = letter.toCharArray();//字符串转换成字符数组
        for (int k = 0; k < 26; k++) {
            X[k] = 0;
            for (i = 0; i < A.length(); i++) {
                NUM[i] = A.charAt(i);
                if (Z[k] == NUM[i] || Z[k] == ch(NUM[i])) {
                    X[k]++;
                }
            }
        }


        double sum = 0;
        for (i = 0; i < 25; i++)
            for (int k = 0; k < 25 - i; k++) {
                if (X[k] < X[k + 1]) {
                    int temp1 = X[k];
                    X[k] = X[k + 1];
                    X[k + 1] = temp1;
                    char temp2 = Z[k];
                    Z[k] = Z[k + 1];
                    Z[k + 1] = temp2;
                }
            }
        for (i = 0; i < 26; i++) {
            sum = sum + X[i];
        }
        for (i = 0; i < 26; i++) {
            float bfb = (float) ((X[i]) / sum * 100);
            System.out.println(Z[i] + "个数:" + X[i] + "频率为：" + num.format(bfb) + "%");
        }

    }
    static char ch(char c) {
        if (!(c >= 97 && c <= 122))
            c += 32;
        return c;
    }


}